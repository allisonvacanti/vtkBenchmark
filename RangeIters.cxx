#include <vtkDataArrayRange.h>

#include <vtkAOSDataArrayTemplate.h>
#include <vtkAssume.h>
#include <vtkDataArray.h>
#include <vtkDataArrayAccessor.h>
#include <vtkFloatArray.h>
#include <vtkNew.h>
#include <vtkSOADataArrayTemplate.h>

#include <benchmark/benchmark.h>

#include <array>
#include <vector>

namespace {

// Use 1GB of 3 component float tuples:
static bool inited = false;
using ValueType = float;
static constexpr vtk::ComponentIdType NumComps = 3;
static constexpr vtk::TupleIdType NumTuples =
    1024 * 1024 * 1024 / (sizeof(ValueType) * NumComps);
constexpr static benchmark::TimeUnit TimeUnit = benchmark::kMillisecond;

// Use to prep arrays:
template <typename ArrayType>
void FillArray(ArrayType *array)
{
  array->SetNumberOfComponents(NumComps);
  array->SetNumberOfTuples(NumTuples);

  using APIType = vtk::GetAPIType<ArrayType>;
  APIType value = 0;

  for (auto& tuple : vtk::DataArrayTupleRange<NumComps>(array))
  {
    for (auto& comp : tuple)
    {
      comp = value++;
    }
  }
}

static vtkNew<vtkAOSDataArrayTemplate<float>> AOSStorage;
static vtkNew<vtkSOADataArrayTemplate<float>> SOAStorage;
static vtkAOSDataArrayTemplate<float>* AOSArray = AOSStorage;
static vtkSOADataArrayTemplate<float>* SOAArray = SOAStorage;
static vtkDataArray *AOSDataArray = AOSStorage;
static vtkDataArray *SOADataArray = SOAStorage;

void Init()
{
  if (!inited)
  {
    FillArray(AOSArray);
    FillArray(SOAArray);
    inited = true;
  }
}

void PrepareState(benchmark::State& state)
{
}

void FinalizeState(benchmark::State& state)
{
  const auto iterations = state.iterations();
  auto bytesPerIteration = NumTuples * NumComps * sizeof(ValueType);
  state.SetBytesProcessed(iterations * bytesPerIteration);
}

#define DO_READ_BENCH_VARSIZE(function, array) \
  static void Read_ ## array ## _ ## function(benchmark::State& state) \
  { \
    Init(); \
    PrepareState(state); \
    for (auto _ : state) \
    { \
      auto result = function(array); \
      benchmark::DoNotOptimize(result); \
    } \
    FinalizeState(state); \
  } \
  BENCHMARK(Read_ ## array ## _ ## function)->Unit(TimeUnit)

#define DO_READ_BENCH_FIXSIZE(function, array) \
  static void Read_ ## array ## _ ## function(benchmark::State& state) \
  { \
    Init(); \
    PrepareState(state); \
    for (auto _ : state) \
    { \
      auto result = function<NumComps>(array); \
      benchmark::DoNotOptimize(result); \
    } \
    FinalizeState(state); \
  } \
  BENCHMARK(Read_ ## array ## _ ## function)->Unit(TimeUnit)

#define DO_WRITE_BENCH_VARSIZE(function, array) \
  static void Write_ ## array ## _ ## function(benchmark::State& state) \
  { \
    Init(); \
    PrepareState(state); \
    for (auto _ : state) \
    { \
      function(array); \
    } \
    FinalizeState(state); \
  } \
  BENCHMARK(Write_ ## array ## _ ## function)->Unit(TimeUnit)

#define DO_WRITE_BENCH_FIXSIZE(function, array) \
  static void Write_ ## array ## _ ## function(benchmark::State& state) \
  { \
    Init(); \
    PrepareState(state); \
    for (auto _ : state) \
    { \
      function<NumComps>(array); \
    } \
    FinalizeState(state); \
  } \
  BENCHMARK(Write_ ## array ## _ ## function)->Unit(TimeUnit)

//==============================================================================
//==============================================================================
// vtkDataArrayAccessor (reads)
//==============================================================================
//==============================================================================

template <typename ArrayType>
auto ReadAccessorTuples(ArrayType *array) -> vtk::GetAPIType<ArrayType>
{
  using Accessor = vtkDataArrayAccessor<ArrayType>;
  using APIType = typename Accessor::APIType;

  const vtk::TupleIdType numTuples = array->GetNumberOfTuples();
  const vtk::ComponentIdType numComps = array->GetNumberOfComponents();

  Accessor a(array);
  APIType sum{0};
  for (vtk::TupleIdType t = 0; t < numTuples; ++t)
  {
    for (vtk::ComponentIdType c = 0; c < numComps; ++c)
    {
      sum += a.Get(t, c);
    }
  }
  return sum;
}

DO_READ_BENCH_VARSIZE(ReadAccessorTuples, AOSDataArray);
DO_READ_BENCH_VARSIZE(ReadAccessorTuples, SOADataArray);
DO_READ_BENCH_VARSIZE(ReadAccessorTuples, AOSArray);
DO_READ_BENCH_VARSIZE(ReadAccessorTuples, SOAArray);

//==============================================================================
//==============================================================================
// vtkDataArrayAccessor + VTK_ASSUME(compSize) (reads)
//==============================================================================
//==============================================================================

template <vtk::ComponentIdType TupleSize, typename ArrayType>
auto ReadAccessorTuplesFixedSize(ArrayType *array) -> vtk::GetAPIType<ArrayType>
{
  using Accessor = vtkDataArrayAccessor<ArrayType>;
  using APIType = typename Accessor::APIType;

  const vtk::TupleIdType numTuples = array->GetNumberOfTuples();

  Accessor a(array);
  APIType sum{0};
  VTK_ASSUME(array->GetNumberOfComponents() == TupleSize);

  for (vtk::TupleIdType t = 0; t < numTuples; ++t)
  {
    for (vtk::ComponentIdType c = 0; c < TupleSize; ++c)
    {
      sum += a.Get(t, c);
    }
  }
  return sum;
}

DO_READ_BENCH_FIXSIZE(ReadAccessorTuplesFixedSize, AOSDataArray);
DO_READ_BENCH_FIXSIZE(ReadAccessorTuplesFixedSize, SOADataArray);
DO_READ_BENCH_FIXSIZE(ReadAccessorTuplesFixedSize, AOSArray);
DO_READ_BENCH_FIXSIZE(ReadAccessorTuplesFixedSize, SOAArray);

//==============================================================================
//==============================================================================
// Raw Pointers (runtime tuple size) (reads)
//==============================================================================
//==============================================================================

template <typename ValueType>
ValueType ReadPointerTuples(vtkAOSDataArrayTemplate<ValueType> *array)
{
  const vtk::TupleIdType numTuples = array->GetNumberOfTuples();
  const vtk::ComponentIdType numComps = array->GetNumberOfComponents();

  const ValueType *data = array->GetPointer(0);
  ValueType sum{0};
  for (vtk::TupleIdType t = 0; t < numTuples; ++t)
  {
    for (vtk::ComponentIdType c = 0; c < numComps; ++c)
    {
      sum += *data++;
    }
  }
  return sum;
}

template <typename ValueType>
ValueType ReadPointerTuples(vtkSOADataArrayTemplate<ValueType> *array)
{
  const vtk::TupleIdType numTuples = array->GetNumberOfTuples();
  const vtk::ComponentIdType numComps = array->GetNumberOfComponents();

  std::vector<const ValueType*> data(numComps);
  for (vtk::ComponentIdType i = 0; i < numComps; ++i)
  {
    data[static_cast<std::size_t>(i)] = array->GetComponentArrayPointer(i);
  }

  ValueType sum{0};
  for (vtk::TupleIdType t = 0; t < numTuples; ++t)
  {
    for (vtk::ComponentIdType c = 0; c < numComps; ++c)
    {
      sum += data[c][t];
    }
  }
  return sum;
}

DO_READ_BENCH_VARSIZE(ReadPointerTuples, AOSArray);
DO_READ_BENCH_VARSIZE(ReadPointerTuples, SOAArray);

//==============================================================================
//==============================================================================
// Raw Pointers (compiletime tuple size) (reads)
//==============================================================================
//==============================================================================

template <vtk::ComponentIdType TupleSize, typename ValueType>
ValueType ReadPointerTuplesFixedSize(vtkAOSDataArrayTemplate<ValueType> *array)
{
  const vtk::TupleIdType numTuples = array->GetNumberOfTuples();

  const ValueType *data = array->GetPointer(0);
  ValueType sum{0};
  for (vtk::TupleIdType t = 0; t < numTuples; ++t)
  {
    for (vtk::ComponentIdType c = 0; c < TupleSize; ++c)
    {
      sum += *data++;
    }
  }
  return sum;
}

template <vtk::ComponentIdType TupleSize, typename ValueType>
ValueType ReadPointerTuplesFixedSize(vtkSOADataArrayTemplate<ValueType> *array)
{
  const vtk::TupleIdType numTuples = array->GetNumberOfTuples();

  std::array<const ValueType*, TupleSize> data;
  for (vtk::ComponentIdType i = 0; i < TupleSize; ++i)
  {
    data[static_cast<std::size_t>(i)] = array->GetComponentArrayPointer(i);
  }

  ValueType sum{0};
  for (vtk::TupleIdType t = 0; t < numTuples; ++t)
  {
    for (vtk::ComponentIdType c = 0; c < TupleSize; ++c)
    {
      sum += data[c][t];
    }
  }
  return sum;
}

DO_READ_BENCH_FIXSIZE(ReadPointerTuplesFixedSize, AOSArray);
DO_READ_BENCH_FIXSIZE(ReadPointerTuplesFixedSize, SOAArray);

//==============================================================================
//==============================================================================
// vtk::DataArrayTupleRange (runtime tuple size) (reads)
//==============================================================================
//==============================================================================

template <typename ArrayType>
auto ReadTupleRange(ArrayType *array) -> vtk::GetAPIType<ArrayType>
{
  using APIType = typename vtk::GetAPIType<ArrayType>;

  const auto range = vtk::DataArrayTupleRange(array);
  APIType sum{0};
  for (const auto& tuple : range)
  {
    for (const auto& comp : tuple)
    {
      sum += comp;
    }
  }
  return sum;
}

DO_READ_BENCH_VARSIZE(ReadTupleRange, AOSDataArray);
DO_READ_BENCH_VARSIZE(ReadTupleRange, SOADataArray);
DO_READ_BENCH_VARSIZE(ReadTupleRange, AOSArray);
DO_READ_BENCH_VARSIZE(ReadTupleRange, SOAArray);

//==============================================================================
//==============================================================================
// vtk::DataArrayTupleRange (compiletime tuple size) (read)
//==============================================================================
//==============================================================================

template <vtk::ComponentIdType TupleSize, typename ArrayType>
auto ReadTupleRangeFixedSize(ArrayType *array) -> vtk::GetAPIType<ArrayType>
{
  using APIType = typename vtk::GetAPIType<ArrayType>;

  const auto range = vtk::DataArrayTupleRange<TupleSize>(array);
  APIType sum{0};
  for (const auto& tuple : range)
  {
    for (const auto& comp : tuple)
    {
      sum += comp;
    }
  }
  return sum;
}

DO_READ_BENCH_FIXSIZE(ReadTupleRangeFixedSize, AOSDataArray);
DO_READ_BENCH_FIXSIZE(ReadTupleRangeFixedSize, SOADataArray);
DO_READ_BENCH_FIXSIZE(ReadTupleRangeFixedSize, AOSArray);
DO_READ_BENCH_FIXSIZE(ReadTupleRangeFixedSize, SOAArray);

//==============================================================================
//==============================================================================
// vtk::DataArrayTupleRange/TupleValue (runtime tuple size) (reads)
//==============================================================================
//==============================================================================

template <typename ArrayType>
auto ReadIterGetTuple(ArrayType *array) -> vtk::GetAPIType<ArrayType>
{
  using APIType = typename vtk::GetAPIType<ArrayType>;

  const auto range = vtk::DataArrayTupleRange(array);

  std::array<APIType, static_cast<size_t>(NumComps)> arr;
  if (NumComps != array->GetNumberOfComponents())
  {
    std::cerr << "Unexpected error.";
    abort();
  }

  APIType sum{0};
  for (const auto& tref: range)
  {
    tref.GetTuple(arr.data());
    for (const auto& comp : arr)
    {
      sum += comp;
    }
  }
  return sum;
}

DO_READ_BENCH_VARSIZE(ReadIterGetTuple, AOSDataArray);
DO_READ_BENCH_VARSIZE(ReadIterGetTuple, SOADataArray);
DO_READ_BENCH_VARSIZE(ReadIterGetTuple, AOSArray);
DO_READ_BENCH_VARSIZE(ReadIterGetTuple, SOAArray);

//==============================================================================
//==============================================================================
// vtk::DataArrayTupleRange/TupleValue (compiletime tuple size) (read)
//==============================================================================
//==============================================================================

template <vtk::ComponentIdType TupleSize, typename ArrayType>
auto ReadIterGetTupleFixedSize(ArrayType *array) -> vtk::GetAPIType<ArrayType>
{
  using APIType = typename vtk::GetAPIType<ArrayType>;

  const auto range = vtk::DataArrayTupleRange<TupleSize>(array);

  std::array<APIType, static_cast<size_t>(TupleSize)> arr;
  APIType sum{0};
  for (const auto& tref : range)
  {
    tref.GetTuple(arr.data());
    for (const auto& comp : arr)
    {
      sum += comp;
    }
  }
  return sum;
}

DO_READ_BENCH_FIXSIZE(ReadIterGetTupleFixedSize, AOSDataArray);
DO_READ_BENCH_FIXSIZE(ReadIterGetTupleFixedSize, SOADataArray);
DO_READ_BENCH_FIXSIZE(ReadIterGetTupleFixedSize, AOSArray);
DO_READ_BENCH_FIXSIZE(ReadIterGetTupleFixedSize, SOAArray);

//==============================================================================
//==============================================================================
// Raw Pointers by value (runtime tuple size) (reads)
//==============================================================================
//==============================================================================

template <typename ValueType>
ValueType ReadPointerValues(vtkAOSDataArrayTemplate<ValueType> *array)
{
  const vtk::ValueIdType numValues = array->GetNumberOfValues();

  const ValueType *data = array->GetPointer(0);
  ValueType sum{0};
  for (vtk::ValueIdType v = 0; v < numValues; ++v)
  {
    sum += *data++;
  }
  return sum;
}

// not really a good way to do this for SOA....
DO_READ_BENCH_VARSIZE(ReadPointerValues, AOSArray);

//==============================================================================
//==============================================================================
// vtk::DataArrayValueRange (runtime tuple size) (reads)
//==============================================================================
//==============================================================================

template <typename ArrayType>
auto ReadValueRange(ArrayType *array) -> vtk::GetAPIType<ArrayType>
{
  using APIType = typename vtk::GetAPIType<ArrayType>;

  const auto range = vtk::DataArrayValueRange(array);
  APIType sum{0};
  for (const auto& value : range)
  {
    sum += value;
  }
  return sum;
}

DO_READ_BENCH_VARSIZE(ReadValueRange, AOSDataArray);
DO_READ_BENCH_VARSIZE(ReadValueRange, SOADataArray);
DO_READ_BENCH_VARSIZE(ReadValueRange, AOSArray);
DO_READ_BENCH_VARSIZE(ReadValueRange, SOAArray);

//==============================================================================
//==============================================================================
// vtk::DataArrayTupleRange (compiletime tuple size) (read)
//==============================================================================
//==============================================================================

template <vtk::ComponentIdType TupleSize, typename ArrayType>
auto ReadValueRangeFixedSize(ArrayType *array) -> vtk::GetAPIType<ArrayType>
{
  using APIType = typename vtk::GetAPIType<ArrayType>;

  const auto range = vtk::DataArrayValueRange<TupleSize>(array);
  APIType sum{0};
  for (const auto& value : range)
  {
    sum += value;
  }
  return sum;
}

DO_READ_BENCH_FIXSIZE(ReadValueRangeFixedSize, AOSDataArray);
DO_READ_BENCH_FIXSIZE(ReadValueRangeFixedSize, SOADataArray);
DO_READ_BENCH_FIXSIZE(ReadValueRangeFixedSize, AOSArray);
DO_READ_BENCH_FIXSIZE(ReadValueRangeFixedSize, SOAArray);

//==============================================================================
//==============================================================================
//==============================================================================
//==============================================================================
// Writes
//==============================================================================
//==============================================================================
//==============================================================================
//==============================================================================

//==============================================================================
//==============================================================================
// vtkDataArrayAccessor (writes)
//==============================================================================
//==============================================================================

template <typename ArrayType>
void WriteAccessorTuples(ArrayType *array)
{
  using Accessor = vtkDataArrayAccessor<ArrayType>;
  using APIType = typename Accessor::APIType;

  const vtk::TupleIdType numTuples = array->GetNumberOfTuples();
  const vtk::ComponentIdType numComps = array->GetNumberOfComponents();

  Accessor a(array);
  APIType value{0};
  for (vtk::TupleIdType t = 0; t < numTuples; ++t)
  {
    for (vtk::ComponentIdType c = 0; c < numComps; ++c)
    {
      a.Set(t, c, value++);
    }
  }
}

DO_WRITE_BENCH_VARSIZE(WriteAccessorTuples, AOSDataArray);
DO_WRITE_BENCH_VARSIZE(WriteAccessorTuples, SOADataArray);
DO_WRITE_BENCH_VARSIZE(WriteAccessorTuples, AOSArray);
DO_WRITE_BENCH_VARSIZE(WriteAccessorTuples, SOAArray);

//==============================================================================
//==============================================================================
// vtkDataArrayAccessor + VTK_ASSUME(compSize) (writes)
//==============================================================================
//==============================================================================

template <vtk::ComponentIdType TupleSize, typename ArrayType>
void WriteAccessorTuplesFixedSize(ArrayType *array)
{
  using Accessor = vtkDataArrayAccessor<ArrayType>;
  using APIType = typename Accessor::APIType;

  const vtk::TupleIdType numTuples = array->GetNumberOfTuples();

  Accessor a(array);
  APIType value{0};
  VTK_ASSUME(array->GetNumberOfComponents() == TupleSize);

  for (vtk::TupleIdType t = 0; t < numTuples; ++t)
  {
    for (vtk::ComponentIdType c = 0; c < TupleSize; ++c)
    {
      a.Set(t, c, value++);
    }
  }
}

DO_WRITE_BENCH_FIXSIZE(WriteAccessorTuplesFixedSize, AOSDataArray);
DO_WRITE_BENCH_FIXSIZE(WriteAccessorTuplesFixedSize, SOADataArray);
DO_WRITE_BENCH_FIXSIZE(WriteAccessorTuplesFixedSize, AOSArray);
DO_WRITE_BENCH_FIXSIZE(WriteAccessorTuplesFixedSize, SOAArray);

//==============================================================================
//==============================================================================
// Raw Pointers (runtime tuple size) (writes)
//==============================================================================
//==============================================================================

template <typename ValueType>
void WritePointerTuples(vtkAOSDataArrayTemplate<ValueType> *array)
{
  const vtk::TupleIdType numTuples = array->GetNumberOfTuples();
  const vtk::ComponentIdType numComps = array->GetNumberOfComponents();

  ValueType *data = array->GetPointer(0);
  ValueType value{0};
  for (vtk::TupleIdType t = 0; t < numTuples; ++t)
  {
    for (vtk::ComponentIdType c = 0; c < numComps; ++c)
    {
      *data++ = value++;
    }
  }
}

template <typename ValueType>
void WritePointerTuples(vtkSOADataArrayTemplate<ValueType> *array)
{
  const vtk::TupleIdType numTuples = array->GetNumberOfTuples();
  const vtk::ComponentIdType numComps = array->GetNumberOfComponents();

  std::vector<ValueType*> data(numComps);
  for (vtk::ComponentIdType i = 0; i < numComps; ++i)
  {
    data[static_cast<std::size_t>(i)] = array->GetComponentArrayPointer(i);
  }

  ValueType value{0};
  for (vtk::TupleIdType t = 0; t < numTuples; ++t)
  {
    for (vtk::ComponentIdType c = 0; c < numComps; ++c)
    {
      data[c][t] = value++;
    }
  }
}

DO_WRITE_BENCH_VARSIZE(WritePointerTuples, AOSArray);
DO_WRITE_BENCH_VARSIZE(WritePointerTuples, SOAArray);

//==============================================================================
//==============================================================================
// Raw Pointers (compiletime tuple size) (writes)
//==============================================================================
//==============================================================================

template <vtk::ComponentIdType TupleSize, typename ValueType>
void WritePointerTuplesFixedSize(vtkAOSDataArrayTemplate<ValueType> *array)
{
  const vtk::TupleIdType numTuples = array->GetNumberOfTuples();

  ValueType *data = array->GetPointer(0);
  ValueType value{0};
  for (vtk::TupleIdType t = 0; t < numTuples; ++t)
  {
    for (vtk::ComponentIdType c = 0; c < TupleSize; ++c)
    {
      *data++ = value++;
    }
  }
}

template <vtk::ComponentIdType TupleSize, typename ValueType>
void WritePointerTuplesFixedSize(vtkSOADataArrayTemplate<ValueType> *array)
{
  const vtk::TupleIdType numTuples = array->GetNumberOfTuples();

  std::array<ValueType*, TupleSize> data;
  for (vtk::ComponentIdType i = 0; i < TupleSize; ++i)
  {
    data[static_cast<std::size_t>(i)] = array->GetComponentArrayPointer(i);
  }

  ValueType value{0};
  for (vtk::TupleIdType t = 0; t < numTuples; ++t)
  {
    for (vtk::ComponentIdType c = 0; c < TupleSize; ++c)
    {
      data[c][t] = value++;
    }
  }
}

DO_WRITE_BENCH_FIXSIZE(WritePointerTuplesFixedSize, AOSArray);
DO_WRITE_BENCH_FIXSIZE(WritePointerTuplesFixedSize, SOAArray);

//==============================================================================
//==============================================================================
// vtk::DataArrayTupleRange (runtime tuple size) (writes)
//==============================================================================
//==============================================================================

template <typename ArrayType>
void WriteTupleRange(ArrayType *array)
{
  using APIType = typename vtk::GetAPIType<ArrayType>;

  auto range = vtk::DataArrayTupleRange(array);
  APIType value{0};
  for (auto& tuple : range)
  {
    for (auto& comp : tuple)
    {
      comp = value++;
    }
  }
}

DO_WRITE_BENCH_VARSIZE(WriteTupleRange, AOSDataArray);
DO_WRITE_BENCH_VARSIZE(WriteTupleRange, SOADataArray);
DO_WRITE_BENCH_VARSIZE(WriteTupleRange, AOSArray);
DO_WRITE_BENCH_VARSIZE(WriteTupleRange, SOAArray);

//==============================================================================
//==============================================================================
// vtk::DataArrayTupleRange (compiletime tuple size) (read)
//==============================================================================
//==============================================================================

template <vtk::ComponentIdType TupleSize, typename ArrayType>
void WriteTupleRangeFixedSize(ArrayType *array)
{
  using APIType = typename vtk::GetAPIType<ArrayType>;

  auto range = vtk::DataArrayTupleRange<TupleSize>(array);
  APIType value{0};
  for (auto& tuple : range)
  {
    for (auto& comp : tuple)
    {
      comp = value++;
    }
  }
}

DO_WRITE_BENCH_FIXSIZE(WriteTupleRangeFixedSize, AOSDataArray);
DO_WRITE_BENCH_FIXSIZE(WriteTupleRangeFixedSize, SOADataArray);
DO_WRITE_BENCH_FIXSIZE(WriteTupleRangeFixedSize, AOSArray);
DO_WRITE_BENCH_FIXSIZE(WriteTupleRangeFixedSize, SOAArray);

//==============================================================================
//==============================================================================
// vtk::DataArrayTupleRange/SetTuple (runtime tuple size) (writes)
//==============================================================================
//==============================================================================

template <typename ArrayType>
void WriteIterGetTuple(ArrayType *array)
{
  using APIType = typename vtk::GetAPIType<ArrayType>;

  auto range = vtk::DataArrayTupleRange(array);

  std::array<APIType, static_cast<size_t>(NumComps)> arr;
  if (NumComps != array->GetNumberOfComponents())
  {
    std::cerr << "Unexpected error.";
    abort();
  }

  APIType value{0};
  for (auto& tref : range)
  {
    for (auto& comp : arr)
    {
      comp = value++;
    }
    tref.SetTuple(arr.data());
  }
}

DO_WRITE_BENCH_VARSIZE(WriteIterGetTuple, AOSDataArray);
DO_WRITE_BENCH_VARSIZE(WriteIterGetTuple, SOADataArray);
DO_WRITE_BENCH_VARSIZE(WriteIterGetTuple, AOSArray);
DO_WRITE_BENCH_VARSIZE(WriteIterGetTuple, SOAArray);

//==============================================================================
//==============================================================================
// vtk::DataArrayTupleRange/SetTuple (compiletime tuple size) (read)
//==============================================================================
//==============================================================================

template <vtk::ComponentIdType TupleSize, typename ArrayType>
void WriteIterGetTupleFixedSize(ArrayType *array)
{
  using APIType = typename vtk::GetAPIType<ArrayType>;

  const vtk::ComponentIdType numComps = array->GetNumberOfComponents();

  auto range = vtk::DataArrayTupleRange(array);
  std::array<APIType, static_cast<size_t>(NumComps)> arr;

  APIType value{0};
  for (auto& tref : range)
  {
    for (auto& comp : arr)
    {
      comp = value++;
    }
    tref.SetTuple(arr.data());
  }
}

DO_WRITE_BENCH_FIXSIZE(WriteIterGetTupleFixedSize, AOSDataArray);
DO_WRITE_BENCH_FIXSIZE(WriteIterGetTupleFixedSize, SOADataArray);
DO_WRITE_BENCH_FIXSIZE(WriteIterGetTupleFixedSize, AOSArray);
DO_WRITE_BENCH_FIXSIZE(WriteIterGetTupleFixedSize, SOAArray);

//==============================================================================
//==============================================================================
// Raw Pointers (runtime tuple size) (writes)
//==============================================================================
//==============================================================================

template <typename ValueType>
void WritePointerValues(vtkAOSDataArrayTemplate<ValueType> *array)
{
  const vtk::ValueIdType numValues = array->GetNumberOfValues();

  ValueType *data = array->GetPointer(0);
  ValueType value{0};
  for (vtk::ValueIdType v = 0; v < numValues; ++v)
  {
    *data++ = value++;
  }
}

DO_WRITE_BENCH_VARSIZE(WritePointerValues, AOSArray);

//==============================================================================
//==============================================================================
// vtk::DataArrayValueRange (runtime tuple size) (writes)
//==============================================================================
//==============================================================================

template <typename ArrayType>
void WriteValueRange(ArrayType *array)
{
  using APIType = typename vtk::GetAPIType<ArrayType>;

  auto range = vtk::DataArrayValueRange(array);
  APIType value{0};
  for (auto& v : range)
  {
    v = value++;
  }
}

DO_WRITE_BENCH_VARSIZE(WriteValueRange, AOSDataArray);
DO_WRITE_BENCH_VARSIZE(WriteValueRange, SOADataArray);
DO_WRITE_BENCH_VARSIZE(WriteValueRange, AOSArray);
DO_WRITE_BENCH_VARSIZE(WriteValueRange, SOAArray);

//==============================================================================
//==============================================================================
// vtk::DataArrayTupleRange (compiletime tuple size) (read)
//==============================================================================
//==============================================================================

template <vtk::ComponentIdType TupleSize, typename ArrayType>
void WriteValueRangeFixedSize(ArrayType *array)
{
  using APIType = typename vtk::GetAPIType<ArrayType>;

  auto range = vtk::DataArrayValueRange<TupleSize>(array);
  APIType value{0};
  for (auto& v : range)
  {
    v = value++;
  }
}

DO_WRITE_BENCH_FIXSIZE(WriteValueRangeFixedSize, AOSDataArray);
DO_WRITE_BENCH_FIXSIZE(WriteValueRangeFixedSize, SOADataArray);
DO_WRITE_BENCH_FIXSIZE(WriteValueRangeFixedSize, AOSArray);
DO_WRITE_BENCH_FIXSIZE(WriteValueRangeFixedSize, SOAArray);

} // end anon namespace

BENCHMARK_MAIN();
